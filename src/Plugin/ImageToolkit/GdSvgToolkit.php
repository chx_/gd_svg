<?php

namespace Drupal\gd_svg\Plugin\ImageToolkit;

use Drupal\system\Plugin\ImageToolkit\GDToolkit;

/**
 * Augments the GD2 toolkit lightly to not error out on SVG files.
 *
 * @ImageToolkit(
 *   id = "gd_svg",
 *   title = @Translation("GD2 image manipulation toolkit with SVG support.")
 * )
 */
class GdSvgToolkit extends GDToolkit {

  /**
   * @var \SimpleXMLElement|FALSE
   */
  protected $svg;

  const FAKESIZE = 10000;

  public function parseFile() {
    if ($this->svg = @simplexml_load_file($this->getSource())) {
      return TRUE;
    }
    return parent::parseFile();
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    return $this->svg || parent::isValid();
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination) {
    return $this->svg ? $this->svg->saveXML($destination) : parent::save($destination);
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight() {
    return $this->svg ? static::FAKESIZE : parent::getHeight();
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth() {
    return $this->svg ? static::FAKESIZE : parent::getWidth();
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    return $this->svg ? 'image/svg+xml' : parent::getMimeType();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedExtensions() {
    return array_merge(parent::getSupportedExtensions(), ['svg']);
  }

  /**
   * {@inheritdoc}
   */
  public function apply($operation, array $arguments = []) {
    // Image operations (crop, resize etc) simply don't make sense for SVG.
    return $this->svg ? TRUE : parent::apply($operation, $arguments);
  }

}
