Do not use this module to allow untrusted users to upload SVG files. You are warned.

This minimal module allows trusted users to upload and work with SVG files as images. 
There is no effect support for them, use CSS as appropriate. https://www.drupal.org/project/svg_image 
module is a good complement to this one, allows rendering SVGs stored in image fields.

Future work would allow whitelisting effects (svg TRUE in the effect plug annotation or some such) and creating an effect out of 
https://github.com/darylldoyle/svg-sanitizer Patches are welcome.

Another easy patch would rename the class to a trait and add support to Imagemagick as well (the imagemagick Acquia Cloud provides doesn't 
support SVG).
